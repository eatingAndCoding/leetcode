package juanjuan.java;

import juanjuan.java.Bean.ListNode;

import java.util.Objects;

/***
 * /**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode(int x) { val = x; }
 * }
 */
public class TwoNumAdd {

    public static ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        ListNode resultNode = new ListNode(0);
        ListNode l1Node = l1;
        ListNode l2Node = l2;
        ListNode nextResultNode = resultNode;
        int add = 0;
        while (null != l1Node && null != l2Node){
            int sum = l1Node.val + l2Node.val + add;
            int re = sum%10;
            nextResultNode.next = new ListNode(re);
            add = sum >= 10 ? 1 : 0;
            nextResultNode = nextResultNode.next;
            l1Node = l1Node.next;
            l2Node = l2Node.next;
        }
        while (null != l1Node && null == l2Node){
            int sum = l1Node.val + add;
            int re = sum%10;
            nextResultNode.next = new ListNode(re);
            add = sum >= 10 ? 1 : 0;
            nextResultNode = nextResultNode.next;
            l1Node = l1Node.next;
        }
        while (null == l1Node && null != l2Node){
            int sum = l2Node.val + add;
            int re = sum%10;
            nextResultNode.next = new ListNode(re);
            add = sum >= 10 ? 1 : 0;
            nextResultNode = nextResultNode.next;
            l2Node = l2Node.next;
        }
        if (add == 1){
            nextResultNode.next = new ListNode(1);
        }
        return resultNode.next;
    }


}
