package juanjuan.java;

public class Third_noRepeatLongest {

    public static int lengthOfLongestSubstring(String s) {
        if (s.length() == 0) return 0;
        int result = 0;
        for (int i = 0; i < s.length(); i++) {
            if (s.substring(0,i).contains(String.valueOf(s.charAt(i)))){
                int subindex = 0;
                for (int j = 0; j < i; j++) {
                    if (s.charAt(j) == s.charAt(i)){
                        subindex = j;
                    }
                }
                String substring = s.substring(subindex+1);
                int resu1 = lengthOfLongestSubstring(substring);
                return resu1 > result ? resu1 : result;
            }else {
                result = result +1;
            }
        }
        return result;
    }

    public static void main(String[] args) {
        int dvs = lengthOfLongestSubstring("dvscd");
        System.out.println(dvs);
    }


    
}
