package juanjuan.java;

import com.google.common.collect.Lists;

import java.util.ArrayList;
import java.util.List;

public class TwoSums {
    public static void main(String[] args) {
        int[] nums = {1,2,3,4,5,6,7,8};
        List<int[]> result = compareTargetEquSum(nums,8);
        for (int[] sum:result) {
            System.out.println(sum[0]);
            System.out.println(sum[1]);
            System.out.println("--------------------");
        }
    }

    private static List<int[]> compareTargetEquSum(int[] nums, int i) {
        ArrayList<int[]> result = Lists.newArrayList();

        //todo code
        for (int j = 0; j < nums.length; j++) {

            for (int k = j+1; k < nums.length; k++) {
                int sum = nums[j] + nums[k];
                if (sum == i){
                    int[] sumEquIndex = {j,k};
                    result.add(sumEquIndex);
                }
            }

        }
        return result;
    }
}
